//
// Gertboard Demo
//
// SPI control code
//
// This code is part of the Gertboard test suite
// These routines access the DA chip
//
//
// Copyright (C) Gert Jan van Loo & Myra VanInwegen 2012
// No rights reserved
// You may treat this program as if it was in the public domain
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//
// Try to strike a balance between keep code simple for
// novice programmers but still have reasonable quality code
//

#include "gb_common.h"
#include "gb_spi.h"


// Set GPIO pins to the right mode
// DEMO GPIO mapping:
//         Function            Mode
// GPIO0=  unused
// GPIO1=  unused
// GPIO4=  unused
// GPIO7=  SPI chip select B   Alt. 0
// GPIO8=  unused
// GPIO9=  SPI MISO            Alt. 0
// GPIO10= SPI MOSI            Alt. 0
// GPIO11= SPI CLK             Alt. 0
// GPIO14= unused
// GPIO15= unused
// GPIO17= unused
// GPIO18= unused
// GPIO21= unused
// GPIO22= unused
// GPIO23= unused
// GPIO24= unused
// GPIO25= unused
//

// For D to A we only need the SPI bus and SPI chip select B
void setup_gpio()
{
   INP_GPIO(7);  SET_GPIO_ALT(7,0);
   INP_GPIO(9);  SET_GPIO_ALT(9,0);
   INP_GPIO(10); SET_GPIO_ALT(10,0);
   INP_GPIO(11); SET_GPIO_ALT(11,0);
} // setup_gpio


//
//  Read ADC input 0 and show as horizontal bar
//
void main(void)
{ int d, chan, dummy;
  int direction = 0;
  int value = 0;
  do {
    printf ("Which channel do you want to test? Type 0 or 1.\n");
    chan  = (int) getchar();
    (void) getchar(); // eat carriage return
  } while (chan != '0' && chan != '1');
  chan = chan - '0';

  printf ("These are the connections for the digital to analogue test:\n");
  printf ("jumper connecting GP11 to SCLK\n");
  printf ("jumper connecting GP10 to MOSI\n");
  printf ("jumper connecting GP9 to MISO\n");
  printf ("jumper connecting GP7 to CSnB\n");
  printf ("Map the I/O sections\n");
  setup_io();

  printf ("activate SPI bus pins\n");
  setup_gpio();

  printf ("Setup SPI bus\n");
  setup_spi();

  // Most likely, the DAC you have installed is an 8 bit one, not 12 bit so 
  // it will ignore that last nibble (4 bits) we send down the SPI interface.
  // So the number that we pass to write_dac will need to be the number
  // want to set (between 0 and 255) multiplied by 16. In hexidecimal,
  // we just put an extra 0 after the number we want to set.
  // So if we want to set the DAC to 64, this is 0x40, so we send 0x400
  // to write_dac.

  // To calculate the voltage we get out, we use this formula from the
  // datasheet: V_out = (d / 256) * 2.048

  printf ("going\n");
  while(1) {
    printf ("while\n");
    if(direction==1){
      printf("Forward");
      for(value=0;value<255;value++) {
        d = value;
        write_dac(0, d*100);
        write_dac(1, d*100);
        //sleep(1);
        //printf("%d\n",d);
      };
    }
    else
    {
      for(value=0;value<255;value++) {
        printf("back");
        d = value;
        write_dac(0, (256-d)*100);
        write_dac(1, (256-d)*100);
        //sleep(1);
        //printf("%d\n",d);
      };
    direction = !direction;
  };
};

  
  
  restore_io();
} // main