//
// Gelvonometer driver code
//
// We have functions for different elements to project.
// The main function takes argv[1]/argv[2] and determines what to project.

#include <signal.h>
#include <math.h>
#include <signal.h>
#include <stdlib.h>

#include "gb_common.h"
#include "gb_spi.h"

const int offset_x = 2048;
const int offset_y = 2048;

float pic_scale = 700;


// Stuff to put in header
void add_point(int x, int y);
void stop_hv();
void stop_laser();
void nice_exit();


// Set GPIO pins to the right mode
//         Function            Mode
// GPIO7=  SPI chip select B   Alt. 0
// GPIO9=  SPI MISO            Alt. 0
// GPIO10= SPI MOSI            Alt. 0
// GPIO11= SPI CLK             Alt. 0

void handSig() {
    //Do a proper exit on Ctrl-C
    exit(0);
}

void nice_exit()
{
    printf("Bye!");
    stop_hv();
    stop_laser();
}

// For D to A we only need the SPI bus and SPI chip select B
void setup_gpio()
{
   INP_GPIO(0);  OUT_GPIO(0);
   INP_GPIO(1);  OUT_GPIO(1);
   INP_GPIO(7);  SET_GPIO_ALT(7,0);
   INP_GPIO(9);  SET_GPIO_ALT(9,0);
   INP_GPIO(10); SET_GPIO_ALT(10,0);
   INP_GPIO(11); SET_GPIO_ALT(11,0);
} // setup_gpio

void start_hv()
{
    GPIO_SET0 = 2;
}

void stop_hv()
{
    GPIO_CLR0 = 2;
}


void start_laser()
{
    int i;
    for(i=0;i<20;i++)
    {
         GPIO_CLR0 = 1;
         usleep(10000);
         GPIO_SET0 = 1;
         usleep(10000);
    }
}

void stop_laser()
{
    GPIO_CLR0 = 1;
    add_point(0, 0);

}

void add_point(int x, int y)
{
    write_dac(0,x);
    write_dac(1,y);
}

void add_scaled_point(float x, float y)
{
    add_point((int)((x*pic_scale) + offset_x), (int)((y*pic_scale) + offset_y));
}

void add_line(float x1, float y1, float x2, float y2)
{
    int i;
    int num_steps = 10;

    float x_length = abs(x2-x1);
    float y_length = abs(y2-y1);

    float line_length = sqrt((x_length*x_length)+(y_length*y_length));
    //printf("Line is %d long", line_length);
    float step_size = line_length/(float)num_steps;


    float x_step = x_length/(float)step_size;
    float y_step = y_length/(float)step_size;

    float origin_x = x1;
    float origin_y = y1;

    for(i=1;i<=num_steps;i++)
    {
            add_point((int)(origin_x+(x_step*i)), (int)(origin_y+(y_step*i)));
    }
}

void right_arrow()
{
    /*
                            2-
                            !    -
                            |       -
        4------------------3,5        >1,7
                            !       -
                            !   -
                            6-

    */

    add_line(2748,2048,2148,2748);
    add_line(2148,2748,2148,2048);
    add_line(2148,2048,1348,2048);
    add_line(1348,2048,2148,2048);
    add_line(2148,2048,2148,1348);
    add_line(2148,1348,2748,2048);

}
void left_arrow()
{
/*
                   -2
                -   ! 
            -       |
        1,7<       3,5-----------------4
            -       !
                -   !
                   -6



*/
    add_line(1348,2048,1548,2748);
    usleep(100);
    add_line(1548,2748,1548,2048);
    usleep(100);
    add_line(1548,2048,2748,2048);
    usleep(100);
    add_line(2748,2048,1548,2048);
    usleep(100);
    add_line(1548,2048,1548,1348);
    usleep(100);
    add_line(1548,1348,1348,2048);
    usleep(100);
}

void ahead_arrow()
{
/*
             / 4 \
           /   !   \
         /     |     \
        3------2------5
               !
               !
               1

*/
    add_line(2048,1348,2048,2048);
    usleep(100);
    add_line(2048,2048,1348,2048);
    usleep(100);
    add_line(1348,2048,2048,2748);
    usleep(100);
    add_line(2048,2748,2748,2048);
    usleep(100);
    add_line(2748,2048,2048,2048);
    usleep(100);
    add_line(2048,2048,2048,1348);
    usleep(100);
}

void roundabout(char* junction)
{
/*

   _
  |\     , - ~ ~ ~ - ,
    \, '               ' ,
   ,                       ,
  ,           ----          ,
 ,               /           ,
 ,            ---            ,
 ,               |           ,
  ,           ---           ,
   ,                       ,
     ,                  , '
       ' - , _ _ _ ,  '





*/
    //Make the circle first
    int R = 700;
    int x=0; int y=R; int d=5-4*R;
    int dA=12; int dB=20-8*R;
    
    while(x < y ){
        add_point(x,y);
        usleep(100);
        if(d<0){
            d=d+dA;
            dB=dB+8;
            }
        else {
            y=y-1;
            d=d+dB;
            dB=dB+16;
            }
        x=x+1;
        dA=dA+8;
    }
    
    //Circle drawing takes a while, so go do the check now.
    add_point(4095,0);
    usleep(10000);
    
    
    // Now add the line at the top left, so it looks like a roundabout
    add_line(1348,2748,1048,2448);
    usleep(100);
    
    // Now add the junction number
    switch (junction[0]) {
        case '1' : usleep(100); add_line(2048,2348,2048,1748);  // Vertical bar.
                   break;
        case '2' : usleep(100); add_line(1748,2348,2348,2348);  // ---
                   usleep(100); add_line(2348,2348,2348,2248);  //   |
                   usleep(100); add_line(2348,2248,1748,1748);  //  /
                   usleep(100); add_line(1748,1748,2348,1748);  // ---
                   break;
        case '3' : usleep(100); add_line(1748,2348,2348,2348);  // ---
                   usleep(100); add_line(2348,2348,2348,2048);  //   |
                   usleep(100); add_line(2348,2048,1748,2048);  // ---
                   usleep(100); add_line(2348,2048,2348,1748);  //   |
                   usleep(100); add_line(2348,1748,1748,1748);  // ---
                   break;
        case '4' : usleep(100); add_line(2048,1748,2048,2348);  //  /|
                   usleep(100); add_line(2048,2348,1748,2048);  // / |
                   usleep(100); add_line(1748,2048,2148,2048);  // --|-
                                                   //   |
                   break;
        case '5' : usleep(100); add_line(2348,2348,1748,2348);  //  -----
                   usleep(100); add_line(1748,2348,1748,2048);  //  |
                   usleep(100); add_line(1748,2048,2348,2048);  //  -----
                   usleep(100); add_line(2348,2048,2348,1748);  //      |
                   usleep(100); add_line(2348,1748,1748,1748);  //  -----
                   break;
    
    }
    
    
}
//
//  Read ADC input 0 and show as horizontal bar
//


void main(int argc, char * const argv[])
{
  // Handle exit codes, so the laser is stopped on a Ctrl-C
  atexit(nice_exit);
  signal (SIGINT, handSig);
  signal (SIGTERM, handSig);
  signal (SIGHUP, handSig);
  signal (SIGQUIT, handSig);

  int d, chan, dummy;
  int direction = 0;
  int value = 0;
  printf ("These are the connections for the digital to analogue test:\n");
  printf ("jumper connecting GP11 to SCLK\n");
  printf ("jumper connecting GP10 to MOSI\n");
  printf ("jumper connecting GP9 to MISO\n");
  printf ("jumper connecting GP7 to CSnB\n");
  printf ("Map the I/O sections\n");
  setup_io();

  printf ("activate SPI bus pins\n");
  setup_gpio();
  start_hv();
  printf ("Setup SPI bus\n");  setup_spi();

  // Most likely, the DAC you have installed is an 8 bit one, not 12 bit so 
  // it will ignore that last nibble (4 bits) we send down the SPI interface.
  // So the number that we pass to write_dac will need to be the number
  // want to set (between 0 and 255) multiplied by 16. In hexidecimal,
  // we just put an extra 0 after the number we want to set.
  // So if we want to set the DAC to 64, this is 0x40, so we send 0x400
  // to write_dac.

  // To calculate the voltage we get out, we use this formula from the
  // datasheet: V_out = (d / 256) * 2.048

  printf ("going\n");
  start_laser();
  while(1) {


        add_point(4095,0);
        usleep(10000);
        
        // argv[1] contains the command, further parameters contain information
        // (if necessary)
        //
        // Valid commands:
        //
        // left  <no arguments>         : Display left arrow.
        // right <no arguments>         : Display right arrow.
        // ahead <no arguments>         : Display ahead arrow
        // round <char junction number> : Display roundabout image with junction no.
        //                                (from 1 to 5).
        
        if(argv[1]=="left") {
            left_arrow();
        }
        else if(argv[1]=="right") {
            right_arrow();
        }
        else if(argv[1]=="ahead") {
            ahead_arrow();
        }
        else if(argv[1]=="round") {
            roundabout(argv[2]);
        };



/*
        add_scaled_point(-1,-1);
        usleep(10000);
        add_scaled_point(-1,1);
        usleep(10000);
        add_scaled_point(1,1);
        usleep(10000);
        add_scaled_point(1,-1);
        usleep(10000);
*/


};

  
  
  restore_io();
} // main
